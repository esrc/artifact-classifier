"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

from SimpleCV import Image
import logging
import os
import yaml

logger = logging.getLogger()

METADATA_FILE = 'artifact.yml'

#------------------------------------------------------------------------------
# Classes

class Artifact(object):
    """
    An artifact is represented by a file system folder structure that contains,
    at minimum, a metadata file named 'artifact.yml'
    """

    def __init__(self, Path):
        """Initializer
        """
        self.path = Path

    def getName(self):
        """
        Get artifact name.
        """
        path = self.path.split(os.sep)
        return path[-1:][0]

    def getMetadata(self):
        """Get metadata as Python object"""
        try:
            path = self.getMetadataFilePath()
            f = open(path)
            data = f.read()
            f.close()
            return yaml.load(data)
        except:
            return {}

    def getMetadataFilePath(self):
        """Return the absolute file system path to the artifact metadata file.
        """
        return self.path + os.sep + METADATA_FILE

    def getProfileImage(self):
        """Get the primary view image. The primary view is typically the front
        view, or first image in the image set."""
        images = self.getProfileImages()
        if len(images) > 0:
            return images[0]
        return None

    def getProfileImages(self):
        """Get a list of all JPG and PNG files in the specified path.
        """
        files = []
        for filename in os.listdir(self.path):
            fn = filename.lower()
            if fn.endswith('jpg') or fn.endswith('png'):
                files.append(self.path + os.sep + fn)
        files.sort()
        return files


#------------------------------------------------------------------------------
# Functions

def getArtifactBackgroundImage(Img):
    """
    Get image with the artifact removed.
    """
    img = Img.toRGB()
    mask = img.dilate(4)
    mask = mask.erode(4)
    mask = mask.binarize().invert()
    blobs = img.findBlobsFromMask(mask=mask)
    try:
        # find the largest blob, which we presume should be the artifact
        b = None
        area = 0
        for blob in blobs:
            if blob.area > area:
                b = blob
        target = b.blobImage()
        return target
    except:
        msg = "Could not find artifact background"
        logger.error(msg, exc_info=True)
        return None

def getArtifactForegroundBlob(Img):
    """
    Get the artifact foreground blob.
    """
    img = Img.toRGB()
    mask = img.dilate(4)
    mask = mask.erode(4)
    mask = mask.binarize()
    blobs = img.findBlobsFromMask(mask=mask)
    try:
        # find the largest blob, which we presume should be the artifact
        b = None
        area = 0
        for blob in blobs:
            if blob.area > area:
                b = blob
        return b
    except:
        msg = "Could not find artifact foreground"
        logger.error(msg, exc_info=True)
        return None

def getArtifactForegroundImage(Img):
    """
    Get image of artifact, with background removed.
    """
    try:
        blob = getArtifactForegroundBlob(Img)
        img = blob.blobImage()
        return img
    except:
        msg = "Could not extract foreground image"
        logger.error(msg, exc_info=True)
        return None

def isArtifact(Path):
    """Determine if a path contains an artifact metadata structure."""
    if os.path.isdir(Path):
        files = os.listdir(Path)
        if METADATA_FILE in files:
            return True
    return False

def loadArtifacts(Path):
    """Load a collection of artifacts from a file system."""
    dirs = os.listdir(Path)
    artifacts = []
    for name in dirs:
        path = Path + os.sep + name
        if isArtifact(path):
            a = Artifact(path)
            artifacts.append(a)
    return artifacts


"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

import classifier
import os
import unittest

#------------------------------------------------------------------------------

class ClassifierTestSuite(unittest.TestSuite):
    """
    Test suite for Classifier.
    """

    def setUp(self):
        """Setup the test environment."""
        self.here = ''
        self.source = self.here + os.sep + 'test'
        self.output = '/tmp/westlake-classifier'
        if not os.path.exists(self.output):
            os.mkdir(self.output)

    def tearDown(self):
        """Tear down the test environment."""

    def test_classify(self):
        """
        The classifier should produce a folder, inferred metadata file, image
        cache folder, and analysis images for each input artifact.
        """
        c = classifier.Classifier()

"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

import unittest
import Westlake.utils as Utils


#------------------------------------------------------------------------------


class UtilsTests(unittest.TestCase):
    """Test cases for utils module"""

    def setUp(self):
        """Setup the test environment.
        """
        pass

    def tearDown(self):
        """Tear down the test environment.
        """
        pass

    def test_getFileName(self):
        """
        It should return the file name portion of the path.
        """
        cases = {
            "/tmp/the/path/to/my/file.txt" : "file.txt",
            "/tmp/path/to/file-name" : "file-name",
            "/tmp/ path /to//file-name" : "file-name",
            "filename" : "filename",
            "/tmp": None,
        }
        for case in cases:
            filename = Utils.getFileName(case)
            self.assertEqual(filename, cases[case])

    def test_resize(self):
        """
        Resize an image.
        """
        pass

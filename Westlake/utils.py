"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

import math
import os


#------------------------------------------------------------------------------

def fitArtifact(Img, H, W):
    """
    Locate the artifact in the image, then reorient and center the image on
    the artifact, scale and crop the image to the specified dimension.
    """
    i = Img.dilate(4)
    i = i.erode(4)
    inv = i.binarize()
    fs = inv.findBlobs()
    try:
        # find the largest feature by area
        f = None
        a = 0
        for feature in fs:
            if feature.area > a:
                f = feature
    except:
        pass

def getFileName(Path):
    """
    Get the filename from the path.
    """
    if os.path.isdir(Path):
        return None
    elements = Path.split(os.sep)
    return elements[-1]

def resize(Img, H, W):
    """
    Locate the item of interest, determine its bounding box.
    Resize the image to the specified dimensions while maintaining the aspect
    ratio.
    """
    h = Img.height
    w = Img.width
    # determine the rescaled dimensions for the image
    if h > w:
        s = math.ceil(W/w)
    if w > h:
        s = math.ceil(H/h)
    if w == h:
        s = math.ceil(W/w)
    # find the largest integer value that produces the desired image size

    # crop the image to fit

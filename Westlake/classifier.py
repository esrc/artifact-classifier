"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

import ConfigParser
import argparse
import datetime
import logging
import os
import sys


#------------------------------------------------------------------------------

class Classifier(object):
    """Computer vision based artifact classifier.
    """

    def __init__(self):
            """Set logging options, create a configuration file parser, command
            line argument parser.
            """
            # configuration parser
            self.config = ConfigParser.SafeConfigParser()
            # configure command line options
            self.parser = argparse.ArgumentParser(description="Analyze, Harvest, process, and post metadata to an Apache Solr index.")
            self.parser.add_argument('config', help="path to configuration file")
            self.parser.add_argument('--analyze',
                                     help="analyze image collection",
                                     action='store_true')
            self.parser.add_argument('--infer',
                                     help="infer concepts, entities, locations from metadata",
                                     action='store_true')
            self.parser.add_argument('--transform',
                                     help="transform inferred data to Solr Input Document format",
                                     action='store_true')
            self.parser.add_argument('--post',
                                     help="post metadata to Apache Solr index",
                                     action='store_true')
            self.parser.add_argument('--loglevel',
                                     help="set the logging level",
                                     choices=['DEBUG','INFO','ERROR'],
                                     )
            # configure logging
            self.logger = logging.getLogger('')
            self.logger.setLevel(logging.INFO)
            self.logFilePath = '/var/log/westlake.log'
            self.logFormat = '%(asctime)s - %(filename)s %(lineno)03d - %(levelname)s - %(message)s'

    def _configureLogging(self):
        """Configure logging
        """
        # console stream handler
        formatter = logging.Formatter(self.logFormat)
        sh = logging.StreamHandler()
        sh.setFormatter(formatter)
        sh.setLevel(logging.INFO)
        self.logger.addHandler(sh)
        # log file handler
        fh = None
        if os.access(self.logFilePath, os.W_OK):
            import logging.handlers as lh
            fh = lh.RotatingFileHandler(self.logFilePath, backupCount=5, maxBytes=67108864) # 64 MB rollover
            fh.setFormatter(formatter)
            fh.setLevel(logging.DEBUG)
            self.logger.addHandler(fh)
        else:
            msg = "Can not open {0} for writing".format(self.logFilePath)
            self.logger.error(msg)
        # override default log level with user specified value
        if self.args.loglevel:
            if self.args.loglevel == 'DEBUG':
                sh.setLevel(logging.DEBUG)
                if fh:
                    fh.setLevel(logging.DEBUG)
            elif self.args.loglevel == 'INFO':
                sh.setLevel(logging.INFO)
                if fh:
                    fh.setLevel(logging.INFO)
            elif self.args.loglevel == 'ERROR':
                sh.setLevel(logging.ERROR)
                if fh:
                    fh.setLevel(logging.ERROR)

    def run(self):
        """Start processing.
        """
        # parse the command line arguments and set logging
        try:
            self.args = self.parser.parse_args()
            self._configureLogging()
            self.logger.info('Started with ' + ' '.join(sys.argv[1:]))
        except Exception, e:
            self.parser.print_help()
            sys.exit(e)
        # load the configuration file
        try:
            self.config.readfp(open(self.args.config))
        except Exception, e:
            self.logger.critical("Could not load the specified configuration file")
            sys.exit(e)
        # start clock
        start = datetime.datetime.now()
        # start image processing
        if self.args.analyze:
            import artifact
            import classifiers
            actions = self.config.get('analyze', 'actions').split(',')
            source = self.config.get('analyze', 'source')
            output = self.config.get('analyze', 'output')
            if not os.path.exists(output):
                os.makedirs(output)
            assert os.path.exists(source), self.logger.error("Source path does not exist: " + source)
            artifacts = artifact.loadArtifacts(source)
            for a in artifacts.itervalues():
                if 'color' in actions:
                    classifiers.color.classify(a, output)
                if 'size' in actions:
                    classifiers.size.classify(a, output)
        if self.args.infer:
            pass
        if self.args.transform:
            pass
        # post metadata to server
        if self.args.post:
            import solr
            solr.run(self.config)
        # stop clock
        delta = datetime.datetime.now() - start
        s = delta.seconds
        hours, remainder = divmod(s, 3600)
        minutes, seconds = divmod(remainder, 60)
        msg = 'Job finished in %s:%s:%s' % (hours, minutes, seconds)
        self.logger.info(msg)


# entry point
if __name__ == '__main__':
    classifier = Classifier()
    classifier.run()

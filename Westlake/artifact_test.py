"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

from SimpleCV import Image
import artifact
import os
import unittest

#------------------------------------------------------------------------------

class ArtifactTestSuite(unittest.TestCase):
    """
    Test suite for Westlake module.
    """

    def setUp(self):
        """
        Setup the test environment.
        """
        here = os.path.abspath(__file__)
        parent = os.path.join(here, os.pardir)
        self.test = parent + os.sep + 'test'
        self.artifacts = []
        for d in os.listdir(self.test):
            path = self.test + os.sep + d
            if artifact.isArtifact(path):
                self.artifacts.append(path)

    def tearDown(self):
        """
        Tear down the test environment.
        """
        pass

    def test_classify(self):
        """
        The classifier should produce a folder, inferred metadata file,
        image cache folder, and analysis images for each input artifact.
        """
        pass

    def test_getArtifactForegroundBlob(self):
        """
        It should return a blob/feature that corresponds with the artifact
        in the image.
        """
        pass

    def test_getArtifactForegroundImage(self):
        """
        It should return an image that represents only the artifact in the
        larger image area.
        """
        cases = [
            'arrow-01.jpg',
            'arrow-02.jpg',
            'arrow-03.jpg',
        ]
        for case in cases:
            img = Image(self.test + os.sep + case)
            blob = artifact.getArtifactForegroundImage(img)
            self.assertNotEqual(blob, None)

    def test_isArtifact(self):
        """
        It should return true when a file system path represents an artifact
        and false when it does not. A file system object represents an artifact
        if it is a directory and it contains, at minimum, a file named
        artifact.yml. The artifact.yml file must contain at minimum an id
        property.
        """
        for a in self.artifacts:
            pass

    def test_loadArtifacts(self):
        """
        It should return a list of artifacts. Each artifact should have an
        absolute path to its source directory.
        """
        artifacts = artifact.loadArtifacts(self.test)
        self.assertEqual(self.artifacts, artifacts)
        self.assertGreater(len(artifacts), 0)


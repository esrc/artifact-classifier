"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

__description__ = "Utility methods for drawing common elements on an image"


def drawColorSwatch(Img, Color, Label=None, BorderThickness=1, Height=32, Width=32, Position=(0,0)):
    """
    Draw a box with the specified color
    """
    dl = Img.getDrawingLayer()

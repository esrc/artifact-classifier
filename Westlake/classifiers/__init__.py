"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

__author__ = 'dmarques'
__description__ = "Artifact property classifiers"

import color
import edge
import shape
import size
import texture

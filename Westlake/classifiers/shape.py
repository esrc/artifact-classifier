"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

import exemplars as Exemplars


# artifact shape exemplars - we will compare our images against these reference
# objects to determine the degree to which they match those shapes
SQUARE = 'exemplar-square.jpg'
CIRCLE = 'exemplar-circular.jpg'
ELLIPSE = 'exemplar-ellipse.jpg'
LOZENGE = 'exemplar-lozenge.jpg'
RECTANGLE = 'exemplar-rectangle.jpg'
TRIANGLE = 'exemplar-triangular.jpg'


exemplars = Exemplars.getExemplars()
shapes = [SQUARE, CIRCLE, ELLIPSE, LOZENGE, RECTANGLE, TRIANGLE]


#------------------------------------------------------------------------------

def classify(Path):
    """Classifies artifact images based on edge properties. Returns inferred
    information as a dictionary."""
    return {}


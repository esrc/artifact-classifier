"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

from SimpleCV import Image
import Westlake.artifact as Artifact
import Westlake.classifiers.size as Size
import os
import unittest

#------------------------------------------------------------------------------

class SizeTests(unittest.TestCase):
    """
    Test cases for size classifier module.
    """

    def setUp(self):
        """Setup the test environment.
        """
        self.path = os.path.dirname(os.path.abspath(__file__))
        self.test = os.path.abspath(os.path.join(self.path, os.pardir)) + os.sep + 'test'
        self.temp = '/tmp/westlake-sizetests'
        if not os.path.exists(self.temp):
            os.mkdir(self.temp)

    def tearDown(self):
        """Tear down the test environment.
        """
        pass

    def test_classify(self):
        """
        It should execute an analysis of the artifact and write a metadata file
        to the specified location.
        """
        source = self.test + os.sep + 'artifacts'
        artifacts = Artifact.loadArtifacts(source)
        for a in artifacts:
            name = a.getName()
            path = self.temp + os.sep + name + '.yml'
            data = Size.classify(a, path)
            self.assertNotEqual(data, None)
            self.assertEquals(os.path.exists(path), True)

    def test_get2DPixelArea(self):
        """
        It should return an integer pixel area for the artifact.  The pixel
        area must be larger than 0.
        """
        cases = {
            'arrow-01.jpg' : None,
            'arrow-02.jpg' : None,
            'arrow-03.jpg' : None,
        }
        for case in cases:
            img = Image(self.test + os.sep + case)
            area = Size.get2DPixelArea(img)
            self.assertNotEqual(area, None)
            self.assertGreater(area, 0)

    def test_get2DPixelHeight(self):
        """
        It should return the maximum pixel height of the artifact in a 2D image
        view. The height is an integer value that is 0 or larger.
        """
        cases = {
            'arrow-01.jpg' : None,
            'arrow-02.jpg' : None,
            'arrow-03.jpg' : None,
        }
        for case in cases:
            img = Image(self.test + os.sep + case)
            height = Size.get2DPixelHeight(img)
            self.assertNotEqual(height, None)
            self.assertGreater(height, 0)

    def test_get2DPixelWidth(self):
        """
        It should return the maximum pixel width of the artifact in a 2D image
        view. The width is an integer value that is 0 or larger.
        """
        cases = {
            'arrow-01.jpg' : None,
            'arrow-02.jpg' : None,
            'arrow-03.jpg' : None,
        }
        for case in cases:
            img = Image(self.test + os.sep + case)
            width = Size.get2DPixelWidth(img)
            self.assertNotEqual(width, None)
            self.assertGreater(width, 0)

    def test_get3DVolume(self):
        """
        It should return the estimated 3D artifact volume, measured in
        milliliters.
        """
        pass

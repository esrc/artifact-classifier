"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

from SimpleCV import Image
import Westlake.artifact as Artifact
import logging
import os
import yaml

logger = logging.getLogger()

#------------------------------------------------------------------------------

def classify(Artifact, Output):
    """
    Classifies artifact images based on their properties. Returns inferred
    information as a dictionary.
    """
    # load inferred data
    inferred = {}
    if os.path.exists(Output):
        f = open(Output)
        inferred = yaml.load(f)
        f.close()
        msg = "Loaded existing metadata file {0}".format(Output)
        logger.debug(msg)
    if not 'size' in inferred:
        inferred['size'] = {}
    # get the artifact image
    img_file = Artifact.getProfileImage()
    img = Image(img_file)
    # area
    inferred['size']['area'] = get2DPixelArea(img)
    # height
    height = get2DPixelHeight(img)
    inferred['size']['height'] = height
    # width
    width = get2DPixelWidth(img)
    inferred['size']['width'] = width
    # volume
    # write inferred data
    try:
        f = open(Output, 'w')
        data = yaml.dump(inferred)
        f.write(data)
        f.close()
    except:
        msg = "Could not write file {0}".format(Output)
        logger.error(msg, exc_info=True)
    return inferred

def get2DPixelArea(Img, View='front'):
    """
    Get the pixel area of the artifact shape.
    """
    a = Artifact.getArtifactForegroundImage(Img)
    area = a.area()
    if not area:
        return 0
    return area

def get2DPixelHeight(Img, View='front'):
    """
    Get the maximum pixel height of the artifact shape. The bounding box method
    does not return the maximum dimension. Consequently, we use the feature
    outline list, then find the minimum and maximum values within the list to
    determine the height.
    """
    a = Artifact.getArtifactForegroundBlob(Img)
    points = a.mContour
    _, y2 = map(max, zip(*points))
    _, y1 = map(min, zip(*points))
    return y2 - y1

def get2DPixelWidth(Img, View='front'):
    """
    Get the maximum pixel width of the artifact shape. The bounding box method
    does not return the maximum dimension. Consequently, we use the feature
    outline list, then find the minimum and maximum values within the list to
    determine the width.
    """
    a = Artifact.getArtifactForegroundBlob(Img)
    points = a.mContour
    x2, _ = map(max, zip(*points))
    x1, _ = map(min, zip(*points))
    return x2 - x1

def get3DVolume(Img):
    """
    Get the estimated 3D volume of the artifact.
    """
    return -1

"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

# edge shape exemplars
CURVED_EXEMPLAR = 'edge-curved.png'
FLAT_EXEMPLAR = 'edge-flat.png'
SERRATED_EXEMPLAR = 'edge-serrated.png'

def classify(Path):
    """Classifies artifact images based on edge properties. Returns inferred
    information as a dictionary."""
    return {}

if __name__ == '__main__':
    pass

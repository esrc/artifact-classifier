"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

from SimpleCV import Image
import Westlake.artifact as Artifact
import Westlake.classifiers.color as Color
import os
import unittest

#------------------------------------------------------------------------------

class ColorTests(unittest.TestCase):
    """
    Test cases for color.py image classifier module.
    """

    def setUp(self):
        """
        Setup the test environment.
        """
        self.path = os.path.dirname(os.path.abspath(__file__))
        self.test = os.path.abspath(os.path.join(self.path, os.pardir)) + os.sep + 'test'
        self.temp = '/tmp/westlake-colortests'
        if not os.path.exists(self.temp):
            os.mkdir(self.temp)

    def tearDown(self):
        """
        Tear down the test environment.
        """
        # shutil.rmtree(self.temp)
        pass

    def test_classify(self):
        """
        Execute a full color analysis and produce a report file.
        """
        source = self.test + os.sep + "artifacts"
        artifacts = Artifact.loadArtifacts(source)
        for a in artifacts:
            name = a.getName()
            path = self.temp + os.sep + name + '.yml'
            data = Color.classify(a, path, True)
            self.assertNotEqual(data, None)
            self.assertEquals(os.path.exists(path), True)

    def test_drawAnalysis(self):
        """
        It should take an analysis record and draw an image with the analytical
        data at the specified file system location.
        """
        source = self.test + os.sep + 'artifacts'
        artifacts = Artifact.loadArtifacts(source)
        for a in artifacts:
            name = a.getName()
            yml_path = self.temp + os.sep + name + '.yml'
            img_path = self.temp + os.sep + name + '.jpg'
            data = Color.classify(a, yml_path, True)
            Color.drawAnalysis(a, data, img_path)
            self.assertEqual(os.path.exists(img_path), True)

    def test_getArtifactPalette(self):
        """
        It should return a list of the N most representative image colors for
        the artifact, encoded in RGB format.
        """
        cases = {
            'arrow-01.jpg' : None,
            'arrow-02.jpg' : None,
            'arrow-03.jpg' : None,
        }
        for case in cases:
            img = Image(self.test + os.sep + case)
            palette = Color.getArtifactPalette(img)
            self.assertNotEqual(palette, None)
            self.assertGreater(len(palette), 0)
            for color in palette:
                pass

    def test_getArtifactPaletteAsHexadecimal(self):
        """
        It should return a list of the N most representative image colors for
        the artifact, encoded in hexadecimal format.
        """
        cases = {
            'arrow-01.jpg' : None,
            'arrow-02.jpg' : None,
            'arrow-03.jpg' : None,
        }
        for case in cases:
            img = Image(self.test + os.sep + case)
            palette = Color.getArtifactPaletteAsHexadecimal(img)
            self.assertNotEqual(palette, None)
            self.assertGreater(len(palette), 0)
            for color in palette:
                self.assertEqual(color.startswith("#"), True)
                self.assertEqual(len(color), 7)

    def test_getArtifactPaletteAsNamedColors(self):
        """
        It should return a list of the N most representative image colors for
        the artifact, encoded as named color values.
        @todo currently not functioning as desired -- need to find named color that is closest to the rgb value
        @see http://www.w3.org/TR/SVG11/types.html#ColorKeywords
        """
        cases = {
            'arrow-01.jpg' : None,
            'arrow-02.jpg' : None,
            'arrow-03.jpg' : None,
        }
        for case in cases:
            img = Image(self.test + os.sep + case)
            palette = Color.getArtifactPaletteAsNamedColors(img)
            self.assertNotEqual(palette, None)
            self.assertGreater(len(palette), 0)
            for color in palette:
                pass

    def test_getArtifactMeanColor(self):
        """
        Get mean color values for the artifact.
        """
        cases = {
            'arrow-01.jpg' : None,
            'arrow-02.jpg' : None,
            'arrow-03.jpg' : None,
        }
        for case in cases:
            pass


"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

__description__ = "Artifact shape exemplars"

import os

def getExemplars():
    """
    Return a dictionary of paths to image files that are exemplars of
    particular artifact shapes.
    """
    here = os.path.dirname(os.path.abspath(__file__))
    exemplars = {}
    files = os.listdir(here)
    for filename in files:
        if filename.endswith('png'):
            names = filename.split('.')
            name = names[0:-1]
            name = ''.join(name)
            exemplars[name] = '' # load the data
    return exemplars

"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

import Westlake.classifiers.exemplars as Exemplars
import os
import unittest

#------------------------------------------------------------------------------

class ExemplarsTests(unittest.TestCase):
    """
    Test cases for exemplars package.
    """

    def setUp(self):
        """
        Setup the test environment.
        """
        pass

    def tearDown(self):
        """
        Tear down the test environment.
        """
        pass

    def test_load(self):
        """
        It should return a dictionary of shape exemplars. The dictionary key
        is the shape name and the value is the path to the exemplar image
        file in PNG format.
        """
        here = os.path.dirname(os.path.abspath(__file__))
        files = os.listdir(here)
        exemplars = []
        for filename in files:
            if filename.lower().endswith('png'):
                exemplars.append(filename)
        results = Exemplars.getExemplars()
        keys = results.keys()
        self.assertNotEqual(results, None)
        self.assertEqual(len(results), len(keys))

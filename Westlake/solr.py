"""
This file is subject to the terms and conditions defined in the LICENSE file,
which is part of this source code package.
"""

import logging
import os
import requests


#------------------------------------------------------------------------------

class SolrIndex(object):
    """Interface for submitting Solr Input Documents to an Apache Solr/Lucene
    index.
    """

    def __init__(self, Url):
        """Initializer
        """
        self.headers = { 'Content-type': 'text/xml; charset=utf-8' }
        self.logger = logging.getLogger()
        self.url = self._getUrl(Url)

    def _getUrl(self, Url):
        """Ensure that the URL ends with a slash character.
        """
        if Url.endswith('/'):
            return Url + 'update'
        else:
            return Url + '/update'

    def commit(self):
        """Commit staged data to the Solr core.
        """
        msg = '<commit expungeDeletes="true"/>'
        resp = requests.post(self.url, msg, headers=self.headers)
        if resp.status_code == 200:
            self.logger.info("Committed staged data to " + SolrIndex)
        else:
            self.logger.error("Something went wrong trying to commit the changes.")
            self.logger.error("\n%s" % resp.text)

    def flush(self):
        """Flush all documents from the Solr index.
        """
        msg = "<delete><query>*:*</query></delete>"
        resp = requests.post(self.url, msg, headers=self.headers)
        if resp.status_code == 200:
            self.logger.info("Flushed data from " + SolrIndex)
        else:
            self.logger.error("Something went wrong trying to submit a request to wipe the index.")
            self.logger.error("\n%s" % resp.text)

    def optimize(self):
        """Optimize data in the Solr index.
        """
        # send command
        msg = '<optimize waitSearcher="false"/>'
        resp = requests.post(self.url, msg, headers=self.headers)
        if resp.status_code == 200:
            self.logger.info("Optimized " + SolrIndex)
        else:
            self.logger.error("Something went wrong trying to optimize the index.")
            self.logger.error("\n%s" % resp.text)

    def post(self, Source):
        """Post Solr Input Documents in the Source directory to the Solr index.
        """
        # check state
        assert os.path.exists(Source), self.logger.error("Source path does not exist: " + Source)
        # post documents
        files = os.listdir(Source)
        for filename in files:
            if filename.endswith(".xml"):
                try:
                    f = open(Source + os.sep + filename)
                    data = f.read()
                    f.close()
                    resp = requests.post(self.url, data=data, headers=self.headers)
                    if resp.status_code == 200:
                        self.logger.info("Posted " + filename)
                    else:
                        self.logger.error("Submission of %s failed with error %s." % (filename, resp.status_code))
                except:
                    self.logger.error("Could not complete post operation for " + filename, exc_info=True)


class SolrInputDocument(object):
    """
    Solr Input Document
    """

    def __init__(self):
        self.doc = None

    def addField(self, Name, Value):
        pass


#------------------------------------------------------------------------------

def run(Params):
    """Post Solr Input Documents to the index and perform maintenance
    operations.
    """
    actions = Params.get("post", "actions").split(",")
    index = Params.get("post", "index")
    source = Params.get("post", "input")
    # create solr object
    solr = SolrIndex(index)
    # execute actions
    if 'flush' in actions:
        solr.flush()
    if 'post' in actions:
        solr.post(source)
    if 'commit' in actions:
        solr.commit()
    if 'optimize' in actions:
        solr.optimize()

WEST-CLASSIFIER: Artifact Classification and Search Tool
========================================================

Artifact classification utility. Employs computer vision to classify a
collection of artifacts by shape, size, color, edge and surface properties.
Generates a Solr Input Document with inferred artifact properties. Merges
other YAML encoded metadata into Solr Input Document. Posts metadata to an
Apache Solr/Lucene index server. Generates an image cache of
multi-resolution thumbnail images

Uses photogrammetry to build a web viewable 3D model of the artifact from
a sequence of images.


Credits
-------

EAC-Indexer is a project of the eScholarship Research Center at the University
of Melbourne. For more information about the project, please contact us at:

 > eScholarship Research Center
 > University of Melbourne
 > Parkville, Victoria
 > Australia
 > www.esrc.unimelb.edu.au

Authors:

 * Davis Marques <davis.marques@unimelb.edu.au>

Thanks:

 * Google Maps API - http://maps.google.com
 * lxml - http://lxml.de
 * Pairtree - https://pypi.python.org/pypi/Pairtree
 * Python - http://www.python.org
 * PyYAML - http://pyyaml.org
 * SimpleCV - http://www.simplecv.org
 * WebColors - https://pypi.python.org/pypi/webcolors/


License
-------

Please see the LICENSE file for license information.


Installation
------------

Requires Python 2.7.x with lxml, pyYAML, Pairtree, SimpleCV libraries.

 * pip install lxml
 * pip install pairtree
 * pip install pyyaml
 * pip install simplecv


Usage
-----


Revision History
----------------

0.1.0

 * First release


Known Issues
------------

